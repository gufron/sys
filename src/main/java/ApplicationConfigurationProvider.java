
import javax.servlet.ServletContext;
import org.ocpsoft.logging.Logger.Level;
import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.config.Direction;
import org.ocpsoft.rewrite.config.Log;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.Lifecycle;
import org.ocpsoft.rewrite.servlet.config.Response;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gufron
 */
@RewriteConfiguration
public class ApplicationConfigurationProvider extends HttpConfigurationProvider {

    @Override
    public Configuration getConfiguration(ServletContext context) {
        return ConfigurationBuilder.begin()
                 .addRule()
               .when(Response.isCommitted().and(Direction.isInbound()))
               .perform(Lifecycle.abort())
                .addRule(Join.path("/login.html").to("/faces/login.xhtml"))
                .addRule(Join.path("/home.html").to("/faces/home.xhtml"))
                .addRule(Join.path("/").to("/faces/index.xhtml"))
                .addRule(Join.path("/customer-create.html").to("/faces/webpage/customer/customerCreate.xhtml"))
                .addRule(Join.path("/userAccount.html").to("/faces/webpage/usermanagement/userAccount.xhtml"))
                .addRule(Join.path("/user-role.html").to("/faces/webpage/usermanagement/userRole.xhtml"))
                .addRule(Join.path("/notauthorized.html").to("/faces/notauthorized.xhtml"))
                .addRule()
                .perform(Log.message(Level.INFO, "Rewrite is active."));
    }

    @Override
    public int priority() {
        return 10;
    }
}
