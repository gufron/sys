/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.domainUI;

import com.ron.sys.domain.*;
import java.io.Serializable;
import javax.persistence.Id;

/**
 *
 * @author gufron
 */

public class RolePermissionsDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Integer roleId;
    private Integer permissionId;
    private String permissionType;
    private MenuDTO menu;

    public RolePermissionsDTO() {
    }

    public RolePermissionsDTO(MenuDTO menu){
        this.menu=menu;
    
    }
    
    public RolePermissionsDTO(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleId != null ? roleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePermissionsDTO)) {
            return false;
        }
        RolePermissionsDTO other = (RolePermissionsDTO) object;
        if ((this.roleId == null && other.roleId != null) || (this.roleId != null && !this.roleId.equals(other.roleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ron.sys.domain.RolePermissions[ roleId=" + roleId + " ]";
    }

    /**
     * @return the menu
     */
    public MenuDTO getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(MenuDTO menu) {
        this.menu = menu;
    }
    
}
