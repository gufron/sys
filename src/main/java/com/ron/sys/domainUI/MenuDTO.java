/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.domainUI;

import com.ron.sys.domain.*;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author gufron
 */

public class MenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer menuid;
    private String menuname;
    private String alias;
    private String link;
    private Integer parentid;
    private Boolean menustatus;
    private Integer level;
    private Integer ordering;
    private String menutype;
    
    private List<MenuDTO> listMenu;

    public MenuDTO() {
    }

    public MenuDTO(String menuname) {
        this.menuname = menuname;
    }

    public MenuDTO(Integer menuid, String menuname) {
        this.menuid = menuid;
        this.menuname = menuname;
    }

    public MenuDTO(Integer menuid, String menuname, String alias, String link) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
    }

    public MenuDTO(Integer menuid, String menuname, String alias, String link, Boolean status) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
        this.menustatus = status;
    }

    public MenuDTO(Integer menuid, String menuname, String alias, String link, Boolean status, Integer level) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
        this.menustatus = status;
        this.level = level;
    }

    public MenuDTO(Integer menuid) {
        this.menuid = menuid;
    }

    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Boolean getMenustatus() {
        return menustatus;
    }

    public void setMenustatus(Boolean menustatus) {
        this.menustatus = menustatus;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getMenutype() {
        return menutype;
    }

    public void setMenutype(String menutype) {
        this.menutype = menutype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuid != null ? menuid.hashCode() : 0);
        return hash;
    }
   
    /**
     * @return the listMenu
     */
    public List<MenuDTO> getListMenu() {
        return listMenu;
    }

    /**
     * @param listMenu the listMenu to set
     */
    public void setListMenu(List<MenuDTO> listMenu) {
        this.listMenu = listMenu;
    }

}
