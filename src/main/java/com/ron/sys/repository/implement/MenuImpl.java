/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.implement;

import com.ron.sys.domain.Menu;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.domainUI.RolePermissionsDTO;
import com.ron.sys.repository.interfacesDAO.MenuDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gufron
 */
@Repository("menuDAO")
public class MenuImpl implements MenuDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void insert(Menu menu) {
        sessionFactory.getCurrentSession().save(menu);
    }

    @Override
    public void update(Menu menu) {
        sessionFactory.getCurrentSession().update(menu);
    }

    @Override
    public void delete(Menu menu) {
        sessionFactory.getCurrentSession().delete(menu);
    }

    @Override
    public Menu caribyid(Menu menu) {
        return (Menu) sessionFactory.getCurrentSession().get(Menu.class, menu.getMenuid());
    }

    @Override
    public List<Menu> caribyCustom(Integer id) {
        String hql = id == null || id == 0 ? "FROM Menu U WHERE U.level = 0" : "FROM Menu U WHERE U.parentid  = :parentid";
        String roleid = "";
        hql = hql + roleid;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        if (id != null) {
            query.setParameter("parentid", id);
        }
        List<Menu> menulist = query.list();
        return menulist;
    }

    @Override
    public List<Menu> getMenu() {
        return sessionFactory.getCurrentSession().createCriteria(Menu.class).list();
    }

    public List<RolePermissionsDTO> caribyRoleid(Integer id) {
        String hql = "select R.role_id,R.permission_id,R.permission_type,M.menuid,M.menuname,M.alias,M.link FROM menu M inner join role_permissions R on M.menuid=R.permission_id where R.role_id=:roleid";
        Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
        query.setParameter("roleid", id);
        List results = query.list();
        List<RolePermissionsDTO> listpermissions = new ArrayList<RolePermissionsDTO>();
        if (results != null && results.size() > 0) {
            for (Object obj : results) {
                Object[] objArr = (Object[]) obj;
                int i = 0;
                RolePermissionsDTO rolepermissions = new RolePermissionsDTO();
                rolepermissions.setRoleId((Integer) objArr[i++]);
                rolepermissions.setPermissionId((Integer) objArr[i++]);
                rolepermissions.setPermissionType((String) objArr[i++]);
                MenuDTO menu = new MenuDTO();
                menu.setMenuid((Integer) objArr[i++]);
                menu.setMenuname((String) objArr[i++]);
                menu.setAlias((String) objArr[i++]);
                menu.setLink((String) objArr[i++]);
                rolepermissions.setMenu(menu);
                listpermissions.add(rolepermissions);
            }
        }
        return listpermissions;
    }

    @Override
    public List<Menu> CariMenuByUserid(Integer userid,Integer parentid) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "select * from getmenubyuserrole(:userid,:parentid)")
                .addEntity(Menu.class)
                .setInteger("userid", userid)
                .setParameter("parentid", parentid, StandardBasicTypes.INTEGER);
        List<Menu> result = query.list();
        return result;
    }

}
