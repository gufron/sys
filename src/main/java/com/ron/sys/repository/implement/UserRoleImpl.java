/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.implement;

import com.ron.sys.domain.Role;
import com.ron.sys.repository.interfacesDAO.UserRoleDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gufron
 */
@Repository("userRoleDAO")
public class UserRoleImpl implements UserRoleDAO {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    public void insert(Role role) {
        sessionFactory.getCurrentSession().save(role);
    }
    
    @Override
    public void update(Role role) {
        sessionFactory.getCurrentSession().update(role);
    }
    
    @Override
    public void delete(Role role) {
        sessionFactory.getCurrentSession().delete(role);
    }
    
    @Override
    public Role caribyid(Role role) {
        return (Role) sessionFactory.getCurrentSession().get(Role.class, role.getRoleId());
    }
   
    @Override
    public List<Role> getUserRole() {
        return sessionFactory.getCurrentSession().createCriteria(Role.class).list();
    }
    
}
