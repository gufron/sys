/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.implement;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.RolePermissions;
import com.ron.sys.repository.interfacesDAO.RolePermissionsDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gufron
 */
@Repository("rolePermissionsDAO")
public class RolePermissionsImpl implements RolePermissionsDAO {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Override
    public void insert(RolePermissions role) {
        sessionFactory.getCurrentSession().save(role);
    }
    
    @Override
    public void update(RolePermissions role) {
        sessionFactory.getCurrentSession().update(role);
    }
    
    @Override
    public void delete(Role role) {
        String hql = "delete FROM RolePermissions r WHERE r.roleId = :roleid";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("roleid", role);
        query.executeUpdate();        
    }  
   
    @Override
    public List<RolePermissions> getUserRole() {
        return sessionFactory.getCurrentSession().createCriteria(RolePermissions.class).list();
    }
    
}
