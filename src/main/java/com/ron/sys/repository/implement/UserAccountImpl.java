/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.implement;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domain.UserAccountRoles;
import com.ron.sys.repository.interfacesDAO.UserAccountDAO;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gufron
 */
@Repository("userAccountDAO")
public class UserAccountImpl implements UserAccountDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void insert(UserAccount user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void update(UserAccount user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public void delete(UserAccount user) {
        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public UserAccount caribyid(UserAccount user) {
        return (UserAccount) sessionFactory.getCurrentSession().get(UserAccount.class, user.getUserId());
    }

    @Override
    public UserAccount caribyUsername(UserAccount user) {
        String hql = "FROM UserAccount U WHERE U.username = :username";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("username", user.getUsername());
        return (UserAccount) query.uniqueResult();
    }

    @Override
    public List<UserAccount> getUserAccount() {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
        cr.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        List result = cr.list();
        return result;
    }

    public void insertUserRole(UserAccountRoles userrole) {
        sessionFactory.getCurrentSession().save(userrole);
    }

    public void deleteUserRole(UserAccount user) {

        String hql = "delete from UserAccountRoles U where U.userId= :user";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("user", user);
        query.executeUpdate();
    }

    @Override
    public List<UserAccountRoles> findUserAccountRolesbyRole(Role role) {
        String hql = "FROM UserAccountRoles U WHERE U.roleId = :roleid";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("roleid", role);
        List list = query.list();
        return list;
    }

}
