/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.interfacesDAO;

import com.ron.sys.domain.Role;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface UserRoleDAO {

    void insert(Role role);

    void update(Role role);

    void delete(Role role);

    Role caribyid(Role role);    

    List<Role> getUserRole();

}
