/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.interfacesDAO;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.RolePermissions;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface RolePermissionsDAO {

    void insert(RolePermissions rolepermission);

    void update(RolePermissions rolepermission);

    void delete(Role role);

    List<RolePermissions> getUserRole();

}
