/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.interfacesDAO;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domain.UserAccountRoles;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface UserAccountDAO {

    void insert(UserAccount user);    

    void update(UserAccount user);

    void delete(UserAccount user);

    UserAccount caribyid(UserAccount user);
    
    UserAccount caribyUsername(UserAccount user);

    List<UserAccount> getUserAccount();
        
    void insertUserRole(UserAccountRoles userrole);
    
    void deleteUserRole(UserAccount user) ;
    
    List<UserAccountRoles> findUserAccountRolesbyRole(Role role); 

}
