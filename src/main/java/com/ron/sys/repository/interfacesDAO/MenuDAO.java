/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.repository.interfacesDAO;

import com.ron.sys.domain.Menu;
import com.ron.sys.domainUI.RolePermissionsDTO;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface MenuDAO {

    void insert(Menu menu);

    void update(Menu menu);

    void delete(Menu menu);

    Menu caribyid(Menu menu);
    
    List<Menu> caribyCustom(Integer id);
    
    List<Menu> CariMenuByUserid(Integer userid ,Integer parentid);
    
    List<RolePermissionsDTO> caribyRoleid(Integer id);

    List<Menu> getMenu();

}
