/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author gufron
 */
@Entity
@Table(name = "menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "menuid")
    private Integer menuid;
    @Column(name = "menuname")
    private String menuname;
    @Column(name = "alias")
    private String alias;
    @Column(name = "link")
    private String link;
    @Column(name = "parentid")
    private Integer parentid;
    @Column(name = "menustatus")
    private Boolean menustatus;
    @Column(name = "level")
    private Integer level;
    @Column(name = "ordering")
    private Integer ordering;
    @Column(name = "menutype")
    private String menutype;
    
    public Menu() {
    }

    public Menu(String menuname) {
        this.menuname = menuname;
    }

    public Menu(Integer menuid, String menuname) {
        this.menuid = menuid;
        this.menuname = menuname;
    }

    public Menu(Integer menuid, String menuname, String alias, String link) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
    }

    public Menu(Integer menuid, String menuname, String alias, String link, Boolean status) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
        this.menustatus = status;
    }

    public Menu(Integer menuid, String menuname, String alias, String link, Boolean status, Integer level) {
        this.menuid = menuid;
        this.menuname = menuname;
        this.alias = alias;
        this.link = link;
        this.menustatus = status;
        this.level = level;
    }

    public Menu(Integer menuid) {
        this.menuid = menuid;
    }

    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public Boolean getMenustatus() {
        return menustatus;
    }

    public void setMenustatus(Boolean menustatus) {
        this.menustatus = menustatus;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getMenutype() {
        return menutype;
    }

    public void setMenutype(String menutype) {
        this.menutype = menutype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuid != null ? menuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menuid == null && other.menuid != null) || (this.menuid != null && !this.menuid.equals(other.menuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ron.sys.domain.Menu[ menuid=" + menuid + " ]";
    }


}
