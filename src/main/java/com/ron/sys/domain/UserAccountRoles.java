/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author gufron
 */
@Entity
@Table(name = "user_account_roles")
@NamedQueries({
    @NamedQuery(name = "UserAccountRoles.findAll", query = "SELECT u FROM UserAccountRoles u"),
    @NamedQuery(name = "UserAccountRoles.findByUserAccountRolesId", query = "SELECT u FROM UserAccountRoles u WHERE u.userAccountRolesId = :userAccountRolesId")})
public class UserAccountRoles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_account_roles_id")
    private Integer userAccountRolesId;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private UserAccount userId;
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Role roleId;

    public UserAccountRoles() {
    }

    public UserAccountRoles(Integer userAccountRolesId) {
        this.userAccountRolesId = userAccountRolesId;
    }

    public Integer getUserAccountRolesId() {
        return userAccountRolesId;
    }

    public void setUserAccountRolesId(Integer userAccountRolesId) {
        this.userAccountRolesId = userAccountRolesId;
    }

    public UserAccount getUserId() {
        return userId;
    }

    public void setUserId(UserAccount userId) {
        this.userId = userId;
    }

    public Role getRoleId() {
        return roleId;
    }

    public void setRoleId(Role roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAccountRolesId != null ? userAccountRolesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccountRoles)) {
            return false;
        }
        UserAccountRoles other = (UserAccountRoles) object;
        if ((this.userAccountRolesId == null && other.userAccountRolesId != null) || (this.userAccountRolesId != null && !this.userAccountRolesId.equals(other.userAccountRolesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ron.sys.domain.UserAccountRoles[ userAccountRolesId=" + userAccountRolesId + " ]";
    }
    
}
