/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author gufron
 */
@Entity
@Table(name = "role_permissions")
@NamedQueries({
    @NamedQuery(name = "RolePermissions.findAll", query = "SELECT r FROM RolePermissions r"),
    @NamedQuery(name = "RolePermissions.findByRolePermissionsid", query = "SELECT r FROM RolePermissions r WHERE r.rolePermissionsid = :rolePermissionsid"),
    @NamedQuery(name = "RolePermissions.findByPermissionId", query = "SELECT r FROM RolePermissions r WHERE r.permissionId = :permissionId"),
    @NamedQuery(name = "RolePermissions.findByPermissionType", query = "SELECT r FROM RolePermissions r WHERE r.permissionType = :permissionType")})
public class RolePermissions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "role_permissionsid")
    private Integer rolePermissionsid;
    @Column(name = "permission_id")
    private Integer permissionId;
    @Column(name = "permission_type")
    private String permissionType;
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    @ManyToOne
    private Role roleId;

    public RolePermissions() {
    }

    public RolePermissions(Integer rolePermissionsid) {
        this.rolePermissionsid = rolePermissionsid;
    }

    public Integer getRolePermissionsid() {
        return rolePermissionsid;
    }

    public void setRolePermissionsid(Integer rolePermissionsid) {
        this.rolePermissionsid = rolePermissionsid;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public Role getRoleId() {
        return roleId;
    }

    public void setRoleId(Role roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolePermissionsid != null ? rolePermissionsid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePermissions)) {
            return false;
        }
        RolePermissions other = (RolePermissions) object;
        if ((this.rolePermissionsid == null && other.rolePermissionsid != null) || (this.rolePermissionsid != null && !this.rolePermissionsid.equals(other.rolePermissionsid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ron.sys.domain.RolePermissions[ rolePermissionsid=" + rolePermissionsid + " ]";
    }
    
}
