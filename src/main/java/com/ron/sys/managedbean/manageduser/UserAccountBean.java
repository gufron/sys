/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.managedbean.manageduser;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domain.UserAccountRoles;
import com.ron.sys.service.interfacesService.UserAccountService;
import com.ron.sys.service.interfacesService.UserRoleService;
import com.ron.sys.util.Encrypt;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author gufron
 */
@ManagedBean
@ViewScoped
@Controller
public class UserAccountBean implements Serializable {

    @Autowired
    UserAccountService userAccountService;

    @Autowired
    UserRoleService userRoleService;

    private List<Role> listrole;
    private List<Role> selectedListrole;

    private List<String> selectedOption;

    private List<UserAccount> listuser;
    private UserAccount user;

    /**
     * Creates a new instance of UserAccountBean
     */
    public UserAccountBean() {
    }

    @PostConstruct
    private void init() {
        initClass();
        initList();
    }

    private void initList() {
        listrole = userRoleService.getUserRole();
        selectedListrole = new ArrayList<Role>();
        listuser = userAccountService.getUserAccount();
        selectedOption = new ArrayList<String>();
    }

    private void initClass() {
        user = new UserAccount();
    }

    public void edit(UserAccount user) {
        this.user = user;
        String decrypt="";
        try {
            decrypt = new Encrypt().decrypt(user.getPassword());
        } catch (Exception ex) {
            Logger.getLogger(UserAccountBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.user.setPassword(decrypt);
        System.out.println("password >>"+user.getPassword());
        for (int i = 0; i < user.getUserAccountRolesList().size(); i++) {
            selectedOption.add(user.getUserAccountRolesList().get(i).getRoleId().getRoleId().toString());
            System.out.println("ready to edit >>" + user.getUserAccountRolesList().get(i).getRoleId().getRoleId().toString());
        }
    }

    public void delete(UserAccount user) {
        this.user = user;        
        userAccountService.delete(user);
        init();

    }

    public void simpanUser() {
        user.setCreatorid(1);
        user.setCreationdate(new Date());
        String password = user.getPassword();
        String hashPassword="";
        try {
            hashPassword = new Encrypt().encrypt(password);
        } catch (Exception ex) {
            Logger.getLogger(UserAccountBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("hasil user roles" + selectedOption.toString());
        //set op user hash password

        List<UserAccountRoles> listtemprole = new ArrayList<UserAccountRoles>();
        for (int i = 0; i < selectedOption.size(); i++) {
            System.out.println("seleted role > " + selectedOption.get(i));
            UserAccountRoles temprole = new UserAccountRoles();
            temprole.setRoleId(new Role(Integer.parseInt(selectedOption.get(i).toString())));
            temprole.setUserId(user);
            System.out.println("hasil seleted >" + temprole.getRoleId().getRoleId());
            listtemprole.add(temprole);
        }

        user.setUserAccountRolesList(listtemprole);
        user.setPassword(hashPassword);
        if (user.getUserId() == null) {
            userAccountService.insert(user);
        } else {
            userAccountService.update(user);
        }
        init();

    }

    /**
     * @return the listuser
     */
    public List<UserAccount> getListuser() {
        return listuser;
    }

    /**
     * @param listuser the listuser to set
     */
    public void setListuser(List<UserAccount> listuser) {
        this.listuser = listuser;
    }

    /**
     * @return the user
     */
    public UserAccount getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserAccount user) {
        this.user = user;
    }

    public List<Role> getListrole() {
        return listrole;
    }

    public void setListrole(List<Role> listrole) {
        this.listrole = listrole;
    }

    public List<Role> getSelectedListrole() {
        return selectedListrole;
    }

    public void setSelectedListrole(List<Role> selectedListrole) {
        this.selectedListrole = selectedListrole;
    }

    /**
     * @return the selectedOption
     */
    public List<String> getSelectedOption() {
        return selectedOption;
    }

    /**
     * @param selectedOption the selectedOption to set
     */
    public void setSelectedOption(List<String> selectedOption) {
        this.selectedOption = selectedOption;
    }

}
