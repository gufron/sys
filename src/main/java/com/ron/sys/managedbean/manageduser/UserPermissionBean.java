/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.managedbean.manageduser;

import com.ron.sys.domain.Menu;
import com.ron.sys.domain.RolePermissions;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.service.interfacesService.MenuService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author gufron
 */
@ManagedBean
@ViewScoped
@Controller
public class UserPermissionBean {

    private TreeNode privilegeUserRootMenuTree;
    private TreeNode privilegeUserMenuTree;
    private MenuDTO privilegeUserRootMenu;
    private TreeNode[] selectedPrivilegeUserMenu;
    private List<RolePermissions> rolePermissions;

    @Autowired
    MenuService menuService;

    public TreeNode getPrivilegeUserRootMenuTree() {
        return privilegeUserRootMenuTree;
    }

    @PostConstruct
    public void init() {
        try {
            generatePrivilegeUserMenuTree();
        } catch (SQLException ex) {
            Logger.getLogger(UserPermissionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generatePrivilegeUserMenuTree() throws SQLException {
        // findUserConfirmationDefault root menu
        //      privilegeUserRootMenu = menuService.caribyCustom(null);

        // createUserConfirmationDefault root tree node menu
        privilegeUserRootMenuTree = new CheckboxTreeNode("Root Menu Tree", null);
        Integer i = null;
        MenuDTO menu = new MenuDTO();
        menu.setMenuid(null);
        //recursively createUserConfirmationDefault tree menu
        privilegeUserMenuTree = createTree(menu, privilegeUserRootMenuTree);

    }

    public TreeNode createTree(MenuDTO menu, TreeNode rootNode) throws SQLException {
        TreeNode newNode = new CheckboxTreeNode(menu, rootNode);
        List<MenuDTO> childNodes1;
        if (menu.getMenuid() == null) {
            childNodes1 = menuService.caribyCustom(null);
        } else {
            childNodes1 = menuService.caribyCustom(menu.getMenuid());
        }
        for (MenuDTO subMenu : childNodes1) {
            TreeNode newNode2 = createTree(subMenu, newNode);
            System.out.println("menu id >>" + subMenu.getMenuid());
        }

        return newNode;
    }

    public void setPrivilegeUserRootMenuTree(TreeNode privilegeUserRootMenuTree) {
        this.privilegeUserRootMenuTree = privilegeUserRootMenuTree;
    }

    public TreeNode getPrivilegeUserMenuTree() {
        return privilegeUserMenuTree;
    }

    public void setPrivilegeUserMenuTree(TreeNode privilegeUserMenuTree) {
        this.privilegeUserMenuTree = privilegeUserMenuTree;
    }

    public MenuDTO getPrivilegeUserRootMenu() {
        return privilegeUserRootMenu;
    }

    public void setPrivilegeUserRootMenu(MenuDTO privilegeUserRootMenu) {
        this.privilegeUserRootMenu = privilegeUserRootMenu;
    }

    public TreeNode[] getSelectedPrivilegeUserMenu() {
        return selectedPrivilegeUserMenu;
    }

    public void setSelectedPrivilegeUserMenu(TreeNode[] selectedPrivilegeUserMenu) {
        this.selectedPrivilegeUserMenu = selectedPrivilegeUserMenu;
    }

    /**
     * Creates a new instance of UserPermissionBean
     */
    public UserPermissionBean() {
    }

}
