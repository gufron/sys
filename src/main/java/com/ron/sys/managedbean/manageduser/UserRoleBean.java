/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.managedbean.manageduser;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.RolePermissions;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.domainUI.RolePermissionsDTO;
import com.ron.sys.service.interfacesService.MenuService;
import com.ron.sys.service.interfacesService.UserAccountService;
import com.ron.sys.service.interfacesService.UserRoleService;
import com.ron.sys.util.JSFMessagesUtil;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author gufron
 */
@ManagedBean
@ViewScoped
@Controller
public class UserRoleBean implements Serializable {

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    MenuService menuService;

    @Autowired
    UserAccountService userAccountService;

    private List<Role> listrole;
    private Role role;
    private TreeNode permissionUserRootMenuTree;
    private TreeNode permissionUserMenuTree;
    private TreeNode[] selectedPermissionUserMenu;
    private List<RolePermissionsDTO> rolePermissions;
    private List<TreeNode> selectedTreeList;

    /**
     * Creates a new instance of RoleBean
     */
    public UserRoleBean() {
    }

    @PostConstruct
    private void init() {
        initClass();
        initList();
    }

    private void initList() {
        listrole = new ArrayList<Role>();
        selectedTreeList= new ArrayList<>();
        listrole = userRoleService.getUserRole();
        rolePermissions = new ArrayList<RolePermissionsDTO>();
        selectedPermissionUserMenu = null;
        try {
            generatePermissionUserMenuTree();
        } catch (SQLException ex) {
            Logger.getLogger(UserRoleBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initClass() {
        role = new Role();
    }

    private void retrieveData() {
        rolePermissions = new ArrayList<RolePermissionsDTO>();
    }

    public void edit(Role role) {
        this.role = role;
        rolePermissions = menuService.caribyRoleid(role.getRoleId());
        convertRolePermissionstoArray();

    }

    public void delete(Role role) {
        this.role = role;
        if (!userAccountService.findUserAccountRolesbyRole(role).isEmpty()) {
            JSFMessagesUtil.sendWarnMessageToUser("Role user terhubung oleh User Account");
            System.out.println("Role user terhubung oleh User Account");
        } else {
            userRoleService.delete(role);
            init();
        }

    }

    private void convertRolePermissionstoArray() {     
        Map<Integer, RolePermissionsDTO> permissionsMap = new HashMap<Integer, RolePermissionsDTO>();
        List<MenuDTO> listMenu = new ArrayList<MenuDTO>();
        if (!rolePermissions.isEmpty()) {
            for (RolePermissionsDTO menu : rolePermissions) {
                if (!menu.getMenu().getLink().equalsIgnoreCase("#") || !menu.getMenu().getLink().equalsIgnoreCase("")) {
                    RolePermissionsDTO temp = new RolePermissionsDTO(menu.getMenu());
                    temp.setPermissionId(menu.getMenu().getMenuid());
                    temp.setPermissionType("menu");
                    listMenu.add(temp.getMenu());
                    permissionsMap.put(menu.getMenu().getMenuid(), temp);
                }
            }
        }
        MenuDTO[] arrayOfMenupermissions = listMenu.toArray(new MenuDTO[listMenu.size()]);

        this.selectedPermissionUserMenu = new TreeNode[arrayOfMenupermissions.length];
        TreeNode root = permissionUserRootMenuTree;
        checkTreeNodes(root, permissionsMap);
        convertArraytoMenu();
        System.out.println("convert to array");
    }

    private void checkTreeNodes(TreeNode root, Map<Integer, RolePermissionsDTO> menuMap) {

        if (root.getChildCount() > 0) {
            root.setExpanded(true);
            for (TreeNode children : root.getChildren()) {

                MenuDTO menu = (MenuDTO) children.getData();
                if (menuMap.containsKey(menu.getMenuid())) {
                    children.setSelected(true);                    
                    selectedTreeList.add(children);
                }
                // recursive check
                checkTreeNodes(children, menuMap);                
            }
        }
        selectedPermissionUserMenu = selectedTreeList.toArray(selectedPermissionUserMenu);
    }

    private void convertArraytoMenu() {
        rolePermissions = new ArrayList<RolePermissionsDTO>();
        List<RolePermissionsDTO> listMenu = new ArrayList<RolePermissionsDTO>();
        for (int i = 0; i < selectedPermissionUserMenu.length; i++) {
            RolePermissionsDTO permissions = new RolePermissionsDTO();
            MenuDTO menu = (MenuDTO) selectedPermissionUserMenu[i].getData();
            permissions.setPermissionId(menu.getMenuid() != null ? menu.getMenuid() : 0);
            permissions.setPermissionType("menu");
            rolePermissions.add(permissions);
        }

    }

    public void generatePermissionUserMenuTree() throws SQLException {
        permissionUserRootMenuTree = new CheckboxTreeNode();
        Integer i = null;
        MenuDTO menu = new MenuDTO();
        permissionUserMenuTree = createTree(menu, permissionUserRootMenuTree);

    }

    public TreeNode createTree(MenuDTO menu, TreeNode rootNode) throws SQLException {
        TreeNode newNode = new CheckboxTreeNode(menu, rootNode);
        List<MenuDTO> childNodes1;
        childNodes1 = menuService.caribyCustom(menu.getMenuid());
        for (MenuDTO subMenu : childNodes1) {
            TreeNode newNode2 = createTree(subMenu, newNode);
        }

        return newNode;
    }

    public void simpan() {
        convertArraytoMenu();
        List<RolePermissions> Permissions = new ArrayList<RolePermissions>();
        for (RolePermissionsDTO rol : rolePermissions) {
            RolePermissions Permissionstemp = new RolePermissions();
            Permissionstemp.setPermissionId(rol.getPermissionId());
            Permissionstemp.setPermissionType(rol.getPermissionType());
            Permissions.add(Permissionstemp);
            System.out.println("list menu checked permission id >>" + rol.getPermissionId());
            System.out.println("list menu checked permission type >>" + rol.getPermissionType());

        }
        if (role.getRoleId() == null) {
            userRoleService.insert(role, Permissions);
        } else {
            userRoleService.update(role, Permissions);
        }
        init();

    }

    /**
     * @return the listrole
     */
    public List<Role> getListrole() {
        return listrole;
    }

    /**
     * @param listrole the listrole to set
     */
    public void setListrole(List<Role> listrole) {
        this.listrole = listrole;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    public TreeNode getPermissionUserRootMenuTree() {
        return permissionUserRootMenuTree;
    }

    public void setPermissionUserRootMenuTree(TreeNode permissionUserRootMenuTree) {
        this.permissionUserRootMenuTree = permissionUserRootMenuTree;
    }

    public TreeNode getPermissionUserMenuTree() {
        return permissionUserMenuTree;
    }

    public void setPermissionUserMenuTree(TreeNode permissionUserMenuTree) {
        this.permissionUserMenuTree = permissionUserMenuTree;
    }

    public TreeNode[] getSelectedPermissionUserMenu() {
        return selectedPermissionUserMenu;
    }

    public void setSelectedPermissionUserMenu(TreeNode[] selectedPermissionUserMenu) {
        this.selectedPermissionUserMenu = selectedPermissionUserMenu;
    }

    public List<RolePermissionsDTO> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(List<RolePermissionsDTO> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}
