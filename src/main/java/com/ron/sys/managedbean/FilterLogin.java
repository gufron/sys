/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.managedbean;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author gufron
 */
public class FilterLogin implements Filter{

    /**
     * Creates a new instance of FilterLogin
     */
    public FilterLogin() {
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
             // Get the loginBean from session attribute
        LoginBean loginBean = (LoginBean)((HttpServletRequest)sr).getSession().getAttribute("loginBean");
         
        // For the first application request there is no loginBean in the session so user needs to log in
        // For other requests loginBean is present but we need to check if user has logged in successfully
        if (loginBean == null || !loginBean.getIsLogin()) {
            String contextPath = ((HttpServletRequest)sr).getContextPath();
            ((HttpServletResponse)sr1).sendRedirect(contextPath + "/login.html");
        System.out.println("contetx pat >>>"+contextPath);
        }
         
        fc.doFilter(sr, sr1);

    }

    @Override
    public void destroy() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
