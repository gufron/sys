/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.managedbean;

import com.ron.sys.domain.Menu;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.service.interfacesService.MenuService;
import com.ron.sys.service.interfacesService.UserAccountService;
import com.ron.sys.util.Encrypt;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author gufron
 */
@ManagedBean(name = "loginBean")
@SessionScoped
@Controller
public class LoginBean implements Serializable {

    private UserAccount user;
    private UserAccount authenticatedUser;
    private Boolean isLogin;
    private String username;
    private String password;
    private List<MenuDTO> listMenu;
    private Map<String, MenuDTO> listMenuCekPermissionsMap;

    @Autowired
    MenuService menuService;

    @Autowired
    UserAccountService userAccountService;

    /**
     * Creates a new instance of LoginBean
     */
    @PostConstruct
    private void init() {
        authenticatedUser = null;
        listMenu = new ArrayList<>();
        listMenuCekPermissionsMap = new HashMap<String, MenuDTO>();
    }

    public LoginBean() {
    }

    public void doLogin() throws Exception {
        UserAccount usertemp = new UserAccount();
        String hashPassword = new Encrypt().encrypt(password);
        this.password = hashPassword;
        usertemp.setUsername(username);
        user = userAccountService.caribyUsername(usertemp);

        if (user != null) {
            // Successful login
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map<String, Object> sessionMap = facesContext.getExternalContext().getSessionMap();
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                authenticatedUser = user;
                isLogin = true;
                sessionMap.put("userLogin", authenticatedUser);
                sessionMap.put("loginBean", this);
                doRedirect("/");
                createMenu();

            } else {
                // Set login ERROR
                FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                sessionMap.remove("userLogin");
                authenticatedUser = null;
            }
        } else {
            FacesMessage msg = new FacesMessage("User Tidak Ditemukan!", "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }

    public void createMenu() {
        listMenu = new ArrayList<MenuDTO>();
        List<MenuDTO> lev0 = menuService.CariMenuByUserid(user.getUserId(), 0);
        List<MenuDTO> lev1 = new ArrayList<MenuDTO>();
        List<MenuDTO> lev2 = new ArrayList<MenuDTO>();
        for (MenuDTO mm0 : lev0) {
            mm0.setListMenu(menuService.CariMenuByUserid(user.getUserId(), mm0.getMenuid()));
            for (MenuDTO mm1 : mm0.getListMenu()) {
                mm1.setListMenu(menuService.CariMenuByUserid(user.getUserId(), mm1.getMenuid()));
                for (MenuDTO mm2 : mm1.getListMenu()) {
                    mm2.setListMenu(menuService.CariMenuByUserid(user.getUserId(), mm2.getMenuid()));
                    System.out.println("menu level 2 >" + mm2.getAlias());
                }
                System.out.println("menu level 1 >" + mm1.getAlias());
            }
            listMenu.add(mm0);
            System.out.println("menu level 0 >" + mm0.getAlias());
        }

        List<MenuDTO> listMenuCekPermissions=menuService.CariMenuByUserid(user.getUserId(), null);
        System.out.println("before size"+menuService.CariMenuByUserid(user.getUserId(), null).size());
        for (MenuDTO menu : listMenuCekPermissions) {
            
            if (!menu.getLink().equalsIgnoreCase("#")) {
                listMenuCekPermissionsMap.put(menu.getAlias(), menu);
            }
            System.out.println("key map menu  >"+listMenuCekPermissionsMap.keySet());
        }
        listMenuCekPermissionsMap.put("/", null);
        System.out.println("key map menu jumlah >"+listMenuCekPermissionsMap.size());

    }

    public void doLogout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map<String, Object> sessionMap = facesContext.getExternalContext().getSessionMap();
        sessionMap.remove("userLogin");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        isLogin = false;
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        doRedirect("/login.html?faces-redirect=true");
    }

    public void verifyUserLogin(ComponentSystemEvent event) {
        if (!isLoggedIn()) {
            doRedirect("/login.html");
        }
    }

    public boolean cekMenuPermissions(String URI) {
        return listMenuCekPermissionsMap.containsKey(URI);
    }

    public boolean isLoggedIn() {
        return (authenticatedUser != null);
    }

    private void doRedirect(String url) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the isLogin
     */
    public Boolean getIsLogin() {
        return isLogin;
    }

    /**
     * @param isLogin the isLogin to set
     */
    public void setIsLogin(Boolean isLogin) {
        this.isLogin = isLogin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the listMenu
     */
    public List<MenuDTO> getListMenu() {
        return listMenu;
    }

    /**
     * @param listMenu the listMenu to set
     */
    public void setListMenu(List<MenuDTO> listMenu) {
        this.listMenu = listMenu;
    }
}
