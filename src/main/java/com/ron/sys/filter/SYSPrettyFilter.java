/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.filter;

//import com.ocpsoft.pretty.PrettyFilter;
import com.ron.sys.managedbean.LoginBean;
import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.ocpsoft.rewrite.servlet.RewriteFilter;

public class SYSPrettyFilter extends RewriteFilter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req1 = (HttpServletRequest) req;

        if (!StringUtils.contains(StringUtils.removeStart(req1.getRequestURI(), req1.getContextPath()), "/resources/") &&
                !StringUtils.contains(StringUtils.removeStart(req1.getRequestURI(), req1.getContextPath()), "/faces/javax.faces.resource/")) {
            if (req1.getSession().getAttribute("loginBean") != null) {

                LoginBean loginBean = (LoginBean) req1.getSession().getAttribute("loginBean");
                System.out.println("do filter berhasils");

                if (loginBean.getIsLogin()) {

                    String requestUri = req1.getRequestURI();
                    requestUri = StringUtils.removeStart(requestUri, req1.getContextPath());

                    System.out.println("---------------------------------------------------");
                    System.out.println("CbsPrettyFilter: contextPath > " + req1.getContextPath());
                    System.out.println("CbsPrettyFilter: requestURI > " + requestUri);
                    System.out.println("CbsPrettyFilter: servletPath > " + req1.getServletPath());
                    System.out.println("---------------------------------------------------");
                    System.out.println(loginBean.cekMenuPermissions(requestUri));
                    if (!loginBean.cekMenuPermissions(requestUri) ) {
                        
//                        // User is not authorized, so redirect to index.
                        HttpServletResponse res = (HttpServletResponse) resp;
//                         try {
//            FacesContext context = FacesContext.getCurrentInstance();
//            context.getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/notauthorized.html?faces-redirect=true");
//        } catch (IOException e) {
//            e.printStackTrace();
//                             System.out.println("gel"+FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/notauthorized.html?faces-redirect=true");
//        }
                        System.out.println("CbsPrettyFilter: notauth berhasil> " + req1.getContextPath()+"/notauthorized.html?faces-redirect=true");
                 //       res.sendError(0, req1.getContextPath() + "/notauthorized.html");
                  //      res.sendRedirect(req1.getContextPath() + "/notauthorized.html");
                        //return;
                    }else{
                    
                    System.out.println("CbsPrettyFilter: notauth gagal > " + req1.getContextPath()+"/notauthorized.html?faces-redirect=true");}

                }

            }
        }

        super.doFilter(req, resp, chain);
    }
}
