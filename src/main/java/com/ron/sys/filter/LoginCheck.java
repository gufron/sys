/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.filter;

import java.util.Map;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import org.slf4j.MDC;


public class LoginCheck implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent pe) {
        FacesContext facesContext = pe.getFacesContext();
        String currentPage = facesContext.getViewRoot().getViewId();

        boolean isLoginPage = currentPage.contains("login");

        Map<String, Object> sessionMap = facesContext.getExternalContext().getSessionMap();
        Object currentUser = sessionMap.get("userLogin");

        if ((currentUser == null || currentUser == "")) {

            if (!isLoginPage) {
                NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
                nh.handleNavigation(facesContext, null, "/login.xhtml?faces-redirect=true");
            }

        } else {
//            OperatorUser operatoruser = (OperatorUser) currentUser;
//            MDC.put("userId", String.valueOf(operatoruser.getUserId()));
        }
    }

    @Override
    public void beforePhase(PhaseEvent pe) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
