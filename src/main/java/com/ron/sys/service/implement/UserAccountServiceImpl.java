/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.service.implement;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domain.UserAccountRoles;
import com.ron.sys.repository.interfacesDAO.UserAccountDAO;
import com.ron.sys.service.interfacesService.UserAccountService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gufron
 */
@Service("userAccountService")
@Transactional(readOnly = true)
public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    UserAccountDAO userAccountDAO;

    @Transactional
    @Override
    public void insert(UserAccount user) {
        userAccountDAO.insert(user);
        System.out.println("user id after insert" + user.getUserId());;
    }

    @Transactional
    @Override
    public void update(UserAccount user) {
        userAccountDAO.deleteUserRole(user);
        userAccountDAO.update(user);
    }

    @Transactional
    @Override
    public void delete(UserAccount user) {
        userAccountDAO.delete(user);
    }

    @Override
    public UserAccount caribyid(UserAccount user) {
        return userAccountDAO.caribyid(user);
    }

    @Override
    public UserAccount caribyUsername(UserAccount user) {
        return userAccountDAO.caribyUsername(user);
    }

    @Override
    public List<UserAccount> getUserAccount() {
        return userAccountDAO.getUserAccount();
    }

    public List<UserAccountRoles> findUserAccountRolesbyRole(Role role) {
        return userAccountDAO.findUserAccountRolesbyRole(role);
    }

}
