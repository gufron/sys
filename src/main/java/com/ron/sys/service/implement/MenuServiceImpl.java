/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.service.implement;

import com.ron.sys.domain.Menu;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.domainUI.RolePermissionsDTO;
import com.ron.sys.repository.interfacesDAO.MenuDAO;
import com.ron.sys.service.interfacesService.MenuService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gufron
 */
@Service("menuService")
@Transactional(readOnly = true)
public class MenuServiceImpl implements MenuService {

    @Autowired
    MenuDAO menuAccountDAO;

    @Transactional
    @Override
    public void insert(MenuDTO menudto) {
        Menu menu = new Menu();
        menuAccountDAO.insert(menu);
    }

    @Transactional
    @Override
    public void update(MenuDTO menudto) {
        Menu menu = new Menu();
        menuAccountDAO.update(menu);
    }

    @Transactional
    @Override
    public void delete(MenuDTO menudto) {
        Menu menu = new Menu();
        menuAccountDAO.delete(menu);
    }

    @Override
    public MenuDTO caribyid(MenuDTO menuDto) {
        Menu menu = new Menu();
        MenuDTO menudto = new MenuDTO();
        menu = menuAccountDAO.caribyid(menu);
        return menudto;
    }

    @Override
    public List<MenuDTO> caribyCustom(Integer id) {
        List<Menu> listmenu = menuAccountDAO.caribyCustom(id);
        List<MenuDTO> listmenudto = new ArrayList<MenuDTO>();
        for (Menu men : listmenu) {
            MenuDTO tempmenuDTO = new MenuDTO(men.getMenuid(), men.getMenuname(), men.getAlias(), men.getLink(), men.getMenustatus(), men.getLevel());
            listmenudto.add(tempmenuDTO);
        }
        return listmenudto;
    }
    
     public List<MenuDTO> CariMenuByUserid(Integer userid,Integer parentid) {
        List<Menu> listmenu = menuAccountDAO.CariMenuByUserid(userid,parentid);
        List<MenuDTO> listmenudto = new ArrayList<MenuDTO>();
        for (Menu men : listmenu) {
            MenuDTO tempmenuDTO = new MenuDTO(men.getMenuid(), men.getMenuname(), men.getAlias(), men.getLink(), men.getMenustatus(), men.getLevel());
            listmenudto.add(tempmenuDTO);            
        }
        return listmenudto;
    }

    @Override
    public List<MenuDTO> getMenu() {
        List<MenuDTO> listmenudto = new ArrayList<MenuDTO>();
        List<Menu> listmenu = new ArrayList<Menu>();
        listmenu = menuAccountDAO.getMenu();
        return listmenudto;
    }

    public List<RolePermissionsDTO> caribyRoleid(Integer id) {
        return menuAccountDAO.caribyRoleid(id);
    }

}
