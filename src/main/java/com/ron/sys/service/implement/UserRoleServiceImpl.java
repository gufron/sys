/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ron.sys.service.implement;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.RolePermissions;
import com.ron.sys.repository.interfacesDAO.RolePermissionsDAO;
import com.ron.sys.repository.interfacesDAO.UserRoleDAO;
import com.ron.sys.service.interfacesService.UserRoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gufron
 */
@Service("userRoleService")
@Transactional(readOnly = true)
public class UserRoleServiceImpl implements UserRoleService {
    
    @Autowired
    UserRoleDAO userRoleDAO;
    
    @Autowired
    RolePermissionsDAO rolePermissionsDAO;
    
    @Transactional
    public void insert(Role role, List<RolePermissions> rolepermissions) {        
        userRoleDAO.insert(role);
        for (RolePermissions rolePermissions : rolepermissions) {
            rolePermissions.setRoleId(role);
            System.out.println("role id >>"+rolePermissions.getRoleId().getRoleId());
            rolePermissionsDAO.insert(rolePermissions);            
        }
    }
    
    @Transactional
    public void update(Role role, List<RolePermissions> rolepermissions) {
        userRoleDAO.update(role);
        rolePermissionsDAO.delete(role);
        for (RolePermissions rolePermissions : rolepermissions) {
            rolePermissions.setRoleId(role);
            System.out.println("role id >>"+rolePermissions.getRoleId().getRoleId());
            rolePermissionsDAO.insert(rolePermissions);            
        }
    }
    
    @Transactional
    public void delete(Role role) {
        rolePermissionsDAO.delete(role);
        userRoleDAO.delete(role);
    }
    
    public Role caribyid(Role role) {
        return userRoleDAO.caribyid(role);
    }
    
    public List<Role> getUserRole() {
        return userRoleDAO.getUserRole();
    }
    
}
