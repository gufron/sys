/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.service.interfacesService;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.RolePermissions;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface UserRoleService {
     void insert(Role role,List<RolePermissions> rolepermissions);

    void update(Role role,List<RolePermissions> rolepermissions);

    void delete(Role role);

    Role caribyid(Role role);    

    List<Role> getUserRole();
    
}
