/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.service.interfacesService;

import com.ron.sys.domain.Role;
import com.ron.sys.domain.UserAccount;
import com.ron.sys.domain.UserAccountRoles;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface UserAccountService {
     void insert(UserAccount user);

    void update(UserAccount user);

    void delete(UserAccount user);

    UserAccount caribyid(UserAccount user);
    
    UserAccount caribyUsername(UserAccount user);

    List<UserAccount> getUserAccount();
    
    List<UserAccountRoles> findUserAccountRolesbyRole(Role role); 
    
}
