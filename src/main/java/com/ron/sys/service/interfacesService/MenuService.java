/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ron.sys.service.interfacesService;

import com.ron.sys.domain.Menu;
import com.ron.sys.domainUI.MenuDTO;
import com.ron.sys.domainUI.RolePermissionsDTO;
import java.util.List;

/**
 *
 * @author gufron
 */
public interface MenuService {
     void insert(MenuDTO menu);

    void update(MenuDTO menu);

    void delete(MenuDTO menu);

    MenuDTO caribyid(MenuDTO menu);
    
    List<MenuDTO> caribyCustom(Integer id);
    
    List<MenuDTO> CariMenuByUserid(Integer userid,Integer parentid);
    
    List<RolePermissionsDTO> caribyRoleid(Integer id);

    List<MenuDTO> getMenu();
    
}
