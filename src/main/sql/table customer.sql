PGDMP                         t         
   DB_LATIHAN    9.1.1    9.1.1 N    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    639176 
   DB_LATIHAN    DATABASE     �   CREATE DATABASE "DB_LATIHAN" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English, Australia' LC_CTYPE = 'English, Australia';
    DROP DATABASE "DB_LATIHAN";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    11638    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    179            �            1259    655065    menu    TABLE       CREATE TABLE menu (
    menuid integer NOT NULL,
    menuname character varying(250),
    alias character varying(150),
    link character varying(150),
    parentid integer,
    menustatus boolean,
    level integer,
    ordering integer,
    menutype character varying(20)
);
    DROP TABLE public.menu;
       public         postgres    false    5            �            1255    687835 #   getmenubyuserrole(integer, integer)    FUNCTION     �  CREATE FUNCTION getmenubyuserrole(userid integer, parentid integer) RETURNS SETOF menu
    LANGUAGE plpgsql
    AS $_$
declare 
id integer :=coalesce($2,0);

begin
if $2 isnull then
return query
select distinct M.*
FROM menu M inner join role_permissions R on M.menuid=R.permission_id
 where R.role_id in(select acr.role_id from user_account ac inner join user_account_roles acr on ac.user_id=acr.user_id where ac.user_id=$1) order by M.menuid;
 else 
 return query
select distinct M.*
FROM menu M inner join role_permissions R on M.menuid=R.permission_id
 where R.role_id in(select acr.role_id from user_account ac inner join user_account_roles acr on ac.user_id=acr.user_id where ac.user_id=$1) and M.parentid=$2 order by M.menuid;

 end if;

end

$_$;
 J   DROP FUNCTION public.getmenubyuserrole(userid integer, parentid integer);
       public       postgres    false    5    522    531            �            1259    642962    hibernate_sequence    SEQUENCE     t   CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public       postgres    false    5            �           0    0    hibernate_sequence    SEQUENCE SET     9   SELECT pg_catalog.setval('hibernate_sequence', 4, true);
            public       postgres    false    163            �            1259    639179    lagu    TABLE     �   CREATE TABLE lagu (
    id integer NOT NULL,
    judul character varying(255) DEFAULT NULL::character varying,
    pencipta character varying(255) DEFAULT NULL::character varying,
    penyanyi character varying(255) DEFAULT NULL::character varying
);
    DROP TABLE public.lagu;
       public         postgres    false    1892    1893    1894    5            �            1259    639177    lagu_id_seq    SEQUENCE     m   CREATE SEQUENCE lagu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.lagu_id_seq;
       public       postgres    false    162    5            �           0    0    lagu_id_seq    SEQUENCE OWNED BY     -   ALTER SEQUENCE lagu_id_seq OWNED BY lagu.id;
            public       postgres    false    161            �           0    0    lagu_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('lagu_id_seq', 1, false);
            public       postgres    false    161            �            1259    642966    mamalia    TABLE     Q   CREATE TABLE mamalia (
    id integer NOT NULL,
    jenis text,
    ciri text
);
    DROP TABLE public.mamalia;
       public         postgres    false    5            �            1259    642964    mamalia_id_seq    SEQUENCE     p   CREATE SEQUENCE mamalia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.mamalia_id_seq;
       public       postgres    false    165    5            �           0    0    mamalia_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE mamalia_id_seq OWNED BY mamalia.id;
            public       postgres    false    164            �           0    0    mamalia_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('mamalia_id_seq', 1, true);
            public       postgres    false    164            �            1259    655063    menu_menuid_seq    SEQUENCE     q   CREATE SEQUENCE menu_menuid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.menu_menuid_seq;
       public       postgres    false    5    174            �           0    0    menu_menuid_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE menu_menuid_seq OWNED BY menu.menuid;
            public       postgres    false    173            �           0    0    menu_menuid_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('menu_menuid_seq', 5, true);
            public       postgres    false    173            �            1259    651213 
   permission    TABLE     j   CREATE TABLE permission (
    permissionid integer NOT NULL,
    permissionname character varying(100)
);
    DROP TABLE public.permission;
       public         postgres    false    5            �            1259    651211    permission_permissionid_seq    SEQUENCE     }   CREATE SEQUENCE permission_permissionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.permission_permissionid_seq;
       public       postgres    false    172    5            �           0    0    permission_permissionid_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE permission_permissionid_seq OWNED BY permission.permissionid;
            public       postgres    false    171            �           0    0    permission_permissionid_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('permission_permissionid_seq', 1, false);
            public       postgres    false    171            �            1259    651185    role    TABLE        CREATE TABLE role (
    role_id integer NOT NULL,
    role_name character varying(100),
    role_code character varying(20)
);
    DROP TABLE public.role;
       public         postgres    false    5            �            1259    663212    role_permissions    TABLE     �   CREATE TABLE role_permissions (
    role_permissionsid integer NOT NULL,
    role_id integer,
    permission_id integer,
    permission_type character varying(50)
);
 $   DROP TABLE public.role_permissions;
       public         postgres    false    5            �            1259    663210 '   role_permissions_role_permissionsid_seq    SEQUENCE     �   CREATE SEQUENCE role_permissions_role_permissionsid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.role_permissions_role_permissionsid_seq;
       public       postgres    false    5    178            �           0    0 '   role_permissions_role_permissionsid_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE role_permissions_role_permissionsid_seq OWNED BY role_permissions.role_permissionsid;
            public       postgres    false    177            �           0    0 '   role_permissions_role_permissionsid_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('role_permissions_role_permissionsid_seq', 227, true);
            public       postgres    false    177            �            1259    651183    role_roleid_seq    SEQUENCE     q   CREATE SEQUENCE role_roleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.role_roleid_seq;
       public       postgres    false    170    5            �           0    0    role_roleid_seq    SEQUENCE OWNED BY     6   ALTER SEQUENCE role_roleid_seq OWNED BY role.role_id;
            public       postgres    false    169            �           0    0    role_roleid_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('role_roleid_seq', 24, true);
            public       postgres    false    169            �            1259    642973 
   tb_mamalia    TABLE     x   CREATE TABLE tb_mamalia (
    id integer NOT NULL,
    ciri character varying(150),
    jenis character varying(150)
);
    DROP TABLE public.tb_mamalia;
       public         postgres    false    5            �            1259    651170    user_account    TABLE     T  CREATE TABLE user_account (
    user_id integer NOT NULL,
    username character varying(100),
    password character varying(100),
    usercode character varying(20),
    fullname character varying(200),
    creatorid integer,
    editorid integer,
    creationdate timestamp without time zone,
    editdate timestamp without time zone
);
     DROP TABLE public.user_account;
       public         postgres    false    5            �            1259    655215    user_account_roles    TABLE     z   CREATE TABLE user_account_roles (
    user_account_roles_id integer NOT NULL,
    user_id integer,
    role_id integer
);
 &   DROP TABLE public.user_account_roles;
       public         postgres    false    5            �            1259    655213 ,   user_account_roles_user_account_roles_id_seq    SEQUENCE     �   CREATE SEQUENCE user_account_roles_user_account_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.user_account_roles_user_account_roles_id_seq;
       public       postgres    false    5    176            �           0    0 ,   user_account_roles_user_account_roles_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE user_account_roles_user_account_roles_id_seq OWNED BY user_account_roles.user_account_roles_id;
            public       postgres    false    175            �           0    0 ,   user_account_roles_user_account_roles_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('user_account_roles_user_account_roles_id_seq', 68, true);
            public       postgres    false    175            �            1259    651173    user_account_userid_seq    SEQUENCE     y   CREATE SEQUENCE user_account_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.user_account_userid_seq;
       public       postgres    false    167    5            �           0    0    user_account_userid_seq    SEQUENCE OWNED BY     F   ALTER SEQUENCE user_account_userid_seq OWNED BY user_account.user_id;
            public       postgres    false    168            �           0    0    user_account_userid_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('user_account_userid_seq', 43, true);
            public       postgres    false    168            c           2604    639182    id    DEFAULT     O   ALTER TABLE lagu ALTER COLUMN id SET DEFAULT nextval('lagu_id_seq'::regclass);
 6   ALTER TABLE public.lagu ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    162    161    162            g           2604    642969    id    DEFAULT     U   ALTER TABLE mamalia ALTER COLUMN id SET DEFAULT nextval('mamalia_id_seq'::regclass);
 9   ALTER TABLE public.mamalia ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    164    165    165            k           2604    655068    menuid    DEFAULT     W   ALTER TABLE menu ALTER COLUMN menuid SET DEFAULT nextval('menu_menuid_seq'::regclass);
 :   ALTER TABLE public.menu ALTER COLUMN menuid DROP DEFAULT;
       public       postgres    false    174    173    174            j           2604    651216    permissionid    DEFAULT     o   ALTER TABLE permission ALTER COLUMN permissionid SET DEFAULT nextval('permission_permissionid_seq'::regclass);
 F   ALTER TABLE public.permission ALTER COLUMN permissionid DROP DEFAULT;
       public       postgres    false    172    171    172            i           2604    651188    role_id    DEFAULT     X   ALTER TABLE role ALTER COLUMN role_id SET DEFAULT nextval('role_roleid_seq'::regclass);
 ;   ALTER TABLE public.role ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    170    169    170            m           2604    663215    role_permissionsid    DEFAULT     �   ALTER TABLE role_permissions ALTER COLUMN role_permissionsid SET DEFAULT nextval('role_permissions_role_permissionsid_seq'::regclass);
 R   ALTER TABLE public.role_permissions ALTER COLUMN role_permissionsid DROP DEFAULT;
       public       postgres    false    177    178    178            h           2604    651175    user_id    DEFAULT     h   ALTER TABLE user_account ALTER COLUMN user_id SET DEFAULT nextval('user_account_userid_seq'::regclass);
 C   ALTER TABLE public.user_account ALTER COLUMN user_id DROP DEFAULT;
       public       postgres    false    168    167            l           2604    655218    user_account_roles_id    DEFAULT     �   ALTER TABLE user_account_roles ALTER COLUMN user_account_roles_id SET DEFAULT nextval('user_account_roles_user_account_roles_id_seq'::regclass);
 W   ALTER TABLE public.user_account_roles ALTER COLUMN user_account_roles_id DROP DEFAULT;
       public       postgres    false    175    176    176            �          0    639179    lagu 
   TABLE DATA               6   COPY lagu (id, judul, pencipta, penyanyi) FROM stdin;
    public       postgres    false    162   �X       �          0    642966    mamalia 
   TABLE DATA               +   COPY mamalia (id, jenis, ciri) FROM stdin;
    public       postgres    false    165   Y       �          0    655065    menu 
   TABLE DATA               g   COPY menu (menuid, menuname, alias, link, parentid, menustatus, level, ordering, menutype) FROM stdin;
    public       postgres    false    174   /Y       �          0    651213 
   permission 
   TABLE DATA               ;   COPY permission (permissionid, permissionname) FROM stdin;
    public       postgres    false    172   �Y       �          0    651185    role 
   TABLE DATA               6   COPY role (role_id, role_name, role_code) FROM stdin;
    public       postgres    false    170   �Y       �          0    663212    role_permissions 
   TABLE DATA               `   COPY role_permissions (role_permissionsid, role_id, permission_id, permission_type) FROM stdin;
    public       postgres    false    178   GZ       �          0    642973 
   tb_mamalia 
   TABLE DATA               .   COPY tb_mamalia (id, ciri, jenis) FROM stdin;
    public       postgres    false    166   �Z       �          0    651170    user_account 
   TABLE DATA               }   COPY user_account (user_id, username, password, usercode, fullname, creatorid, editorid, creationdate, editdate) FROM stdin;
    public       postgres    false    167   �Z       �          0    655215    user_account_roles 
   TABLE DATA               N   COPY user_account_roles (user_account_roles_id, user_id, role_id) FROM stdin;
    public       postgres    false    176   �[       o           2606    639190 	   lagu_pkey 
   CONSTRAINT     E   ALTER TABLE ONLY lagu
    ADD CONSTRAINT lagu_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.lagu DROP CONSTRAINT lagu_pkey;
       public         postgres    false    162    162                       2606    655073 	   menu_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (menuid);
 8   ALTER TABLE ONLY public.menu DROP CONSTRAINT menu_pkey;
       public         postgres    false    174    174            {           2606    651220    permission_permissionname_key 
   CONSTRAINT     f   ALTER TABLE ONLY permission
    ADD CONSTRAINT permission_permissionname_key UNIQUE (permissionname);
 R   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_permissionname_key;
       public         postgres    false    172    172            }           2606    651218    permission_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (permissionid);
 D   ALTER TABLE ONLY public.permission DROP CONSTRAINT permission_pkey;
       public         postgres    false    172    172            �           2606    663217    role_permissions_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT role_permissions_pkey PRIMARY KEY (role_permissionsid);
 P   ALTER TABLE ONLY public.role_permissions DROP CONSTRAINT role_permissions_pkey;
       public         postgres    false    178    178            w           2606    651190 	   role_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (role_id);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public         postgres    false    170    170            y           2606    651192    role_rolename_key 
   CONSTRAINT     O   ALTER TABLE ONLY role
    ADD CONSTRAINT role_rolename_key UNIQUE (role_name);
 @   ALTER TABLE ONLY public.role DROP CONSTRAINT role_rolename_key;
       public         postgres    false    170    170            q           2606    642977    tb_mamalia_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY tb_mamalia
    ADD CONSTRAINT tb_mamalia_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.tb_mamalia DROP CONSTRAINT tb_mamalia_pkey;
       public         postgres    false    166    166            s           2606    651180    user_account_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY user_account
    ADD CONSTRAINT user_account_pkey PRIMARY KEY (user_id);
 H   ALTER TABLE ONLY public.user_account DROP CONSTRAINT user_account_pkey;
       public         postgres    false    167    167            �           2606    655220    user_account_roles_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY user_account_roles
    ADD CONSTRAINT user_account_roles_pkey PRIMARY KEY (user_account_roles_id);
 T   ALTER TABLE ONLY public.user_account_roles DROP CONSTRAINT user_account_roles_pkey;
       public         postgres    false    176    176            u           2606    651182    user_account_username_key 
   CONSTRAINT     ^   ALTER TABLE ONLY user_account
    ADD CONSTRAINT user_account_username_key UNIQUE (username);
 P   ALTER TABLE ONLY public.user_account DROP CONSTRAINT user_account_username_key;
       public         postgres    false    167    167            �           2606    655236    fk2fcerdd36y5il9hbpouxdbxuy    FK CONSTRAINT     �   ALTER TABLE ONLY user_account_roles
    ADD CONSTRAINT fk2fcerdd36y5il9hbpouxdbxuy FOREIGN KEY (user_id) REFERENCES user_account(user_id);
 X   ALTER TABLE ONLY public.user_account_roles DROP CONSTRAINT fk2fcerdd36y5il9hbpouxdbxuy;
       public       postgres    false    1906    167    176            �           2606    655231    fkhcdb6qcknc98ttigom8fou8xh    FK CONSTRAINT     �   ALTER TABLE ONLY user_account_roles
    ADD CONSTRAINT fkhcdb6qcknc98ttigom8fou8xh FOREIGN KEY (role_id) REFERENCES role(role_id);
 X   ALTER TABLE ONLY public.user_account_roles DROP CONSTRAINT fkhcdb6qcknc98ttigom8fou8xh;
       public       postgres    false    176    170    1910            �           2606    687814    fklodb7xh4a2xjv39gc3lsop95n    FK CONSTRAINT     �   ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT fklodb7xh4a2xjv39gc3lsop95n FOREIGN KEY (role_id) REFERENCES role(role_id);
 V   ALTER TABLE ONLY public.role_permissions DROP CONSTRAINT fklodb7xh4a2xjv39gc3lsop95n;
       public       postgres    false    178    170    1910            �           2606    663233    role_permissions_role_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT role_permissions_role_id_fkey FOREIGN KEY (role_id) REFERENCES role(role_id);
 X   ALTER TABLE ONLY public.role_permissions DROP CONSTRAINT role_permissions_role_id_fkey;
       public       postgres    false    170    1910    178            �           2606    655226    user_account_roles_role_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY user_account_roles
    ADD CONSTRAINT user_account_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES role(role_id);
 \   ALTER TABLE ONLY public.user_account_roles DROP CONSTRAINT user_account_roles_role_id_fkey;
       public       postgres    false    176    1910    170            �           2606    655221    user_account_roles_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY user_account_roles
    ADD CONSTRAINT user_account_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES user_account(user_id);
 \   ALTER TABLE ONLY public.user_account_roles DROP CONSTRAINT user_account_roles_user_id_fkey;
       public       postgres    false    167    1906    176            �   /   x�3�L��KL�LJ�.��,,J�2�,I-�I�)�,��M\1z\\\ -       �      x�3�,I-�L�,������ %��      �   �   x�3�t.-.��M-�TB� ��".#����"�ļ����ԼƜ�E��%�
p3���,�d��^FIn�~Zbrj�~yjR�(�8b�^X�!�C�&0@.��/������y%X����b��٦���BL�-2�5���1z\\\ �Pm�      �      x������ � �      �   ;   x�32�,I-�442�22�LJ,*�4���2�LL����400�2�,�L-O-r��b���� S4      �   d   x�M�;�0�99J�܅�6�O��[���5K�H龞��2;�?P�X[bR��j!e����Y�J(C��M!�h��/��?�~��3h�2�      �       x�3�L*�)U0��J�+I��2F���qqq �1
O      �   �   x��ϱm�0���enMܑG��!2����Q �3D��e	�&�Ĕ�0 ��x���ĄRz�5K�˙�D�k�c�V��@�{��h��:r�|rN[q-Rl!�/,���q�|-������������(�O����E�����a`�QF�� ׆�-RKfB�Q��ӿ��um��	Ys\#e"<���m�_^����rc �^�R�
��S�      �   <   x�-���0Cѳ=LD��.����ӷ��g@�|J���n�������!�����
�     